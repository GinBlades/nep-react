import React, { Component } from 'react';
import items from "./nep-nep-item-list.json";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      displayedItems: items
    }
  }

  iname = (prop, val) => (prop.toLowerCase().includes(val.toLowerCase()));

  search = evt => {
    const val = evt.target.value;
    this.setState({
      search: val,
      displayedItems: items.filter(i => {
        return this.iname(i.name, val)
          || this.iname(i.drops, val)
          || this.iname(i.location, val);
      })
    })
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="field">
          </div>
          <label htmlFor="search" className="label">Search</label>
          <div className="control">
            <input className="input" type="text" placeholder="Search anything"
              value={this.state.search} onChange={this.search} />
          </div>

          <table className="table is-bordered is-striped">
            <thead>
              <tr>
                <th>Category</th>
                <th>Name</th>
                <th>Drops</th>
                <th>Location</th>
                <th>Add Enemies</th>
                <th>Super Enemies</th>
              </tr>
            </thead>
            <tbody>
              {this.state.displayedItems.map(i => (<tr>
                <td>{i.category}</td>
                <td>{i.name}</td>
                <td>{i.drops}</td>
                <td>{i.location}</td>
                <td>{i.add_enemies ? "Yes" : ""}</td>
                <td>{i.super_enemies ? "Yes" : ""}</td>
              </tr>))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default App;
